/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader.service;

import com.ideasagiles.reader.domain.Feed;
import java.util.List;

public interface FeedService {

    void save(Feed feed);
    List<Feed> findAll();
    void delete(long feedId);
    void update(Feed feed);

}
