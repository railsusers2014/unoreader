/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader.service.impl;

import com.ideasagiles.reader.domain.Favourite;
import com.ideasagiles.reader.domain.User;
import com.ideasagiles.reader.repository.FavouriteRepository;
import com.ideasagiles.reader.service.FavouriteService;
import com.ideasagiles.reader.service.UserService;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class FavouriteServiceImpl implements FavouriteService {

    @Autowired
    private UserService userService;
    @Autowired
    private FavouriteRepository favouriteRepository;

    @PreAuthorize("isAuthenticated()")
    @Override
    public List<Favourite> findAll() {
        User user = userService.findLoggedInUser();
        return favouriteRepository.findByUserOrderByCreatedOnDesc(user);
    }

    @PreAuthorize("isAuthenticated()")
    @Override
    public void save(Favourite favourite) {
        User user = userService.findLoggedInUser();
        favourite.setUser(user);
        favourite.setCreatedOn(new Date());
        favouriteRepository.save(favourite);
    }

    @Override
    public void delete(Long id) {
        User user = userService.findLoggedInUser();
        Favourite favourite = favouriteRepository.findOne(id);
        if (favourite != null && favourite.getUser().equals(user)) {
            favouriteRepository.delete(favourite);
        }
        else {
            throw new AccessDeniedException("Favourite does not belong to user.");
        }
    }

}
