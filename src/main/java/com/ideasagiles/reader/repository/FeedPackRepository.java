/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader.repository;

import com.ideasagiles.reader.domain.FeedPack;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface FeedPackRepository extends CrudRepository<FeedPack, Long> {

    List<FeedPack> findByPack(String pack);
}
