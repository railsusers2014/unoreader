<%@page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <%--
            Set base for all urls. The base MUST is an absolute URI.
            Read more: http://stackoverflow.com/questions/6271075/how-to-get-the-base-url-from-jsp-request-object
        --%>
        <c:set var="req" value="${pageContext.request}" />
        <c:set var="uri" value="${req.requestURI}" />
        <base href="${fn:replace(req.requestURL, fn:substring(uri, 0, fn:length(uri)), req.contextPath)}/">

        <title><tiles:getAsString name="title"/></title>

        <tiles:importAttribute name="checkBrowserSupport" scope="page"/>
        <c:if test="${checkBrowserSupport == 'true'}">
            <!--[if lt IE 9]>
                <script type="text/javascript">
                    //Redirect IE6, IE7 and IE8
                    window.location = 'notsupported.html';
                </script>
            <![endif]-->
        </c:if>

        <meta name="keywords" content="newsfeed reader, reader, web reader, feed, rss reader, atom reader, news reader, google reader, google reader alternative" />
        <meta name="description" content="A web-based newsfeed reader. Free, elegant and simple to use, it stores your feeds in the cloud so you can access them from any device, anywhere." />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="img/favicon.ico" rel="shortcut icon">

        <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet">
        <link href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
        <!--[if IE 7]>
        <link href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome-ie7.css" rel="stylesheet">
        <![endif]-->

        <link href='http://code.jquery.com/ui/1.10.3/themes/start/jquery-ui.min.css' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700,300italic' rel='stylesheet' type='text/css'>
        <link href="js/lib/jquery-context-menu/jquery.contextMenu.css" rel="stylesheet" type='text/css'>
        <link href="js/lib/jquery-custom-scrollbar/jquery.mCustomScrollbar.css" rel="stylesheet" type='text/css'>
        <link href="css/main.css" rel="stylesheet" type='text/css'>

    </head>
    <body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-39883237-1', 'unoreader.com');
  ga('send', 'pageview');

</script>

        <div class="navbar navbar-inverse navbar-top ur-light">
            <div class="navbar-inner">
                <div class="container<tiles:getAsString name="cssClassSuffix"/>">
                    <a class="brand" href="<c:url value="/"/>">
                        <div class="ur-brand-top"><span class="ur-brand">uno<span>.</span>reader</div>
                        <div class="ur-brand-bottom">beta</div>
                    </a>
                    <div class="pull-right">
                        <ul class="nav ur-top-account">
                            <sec:authorize access="!isAuthenticated()">
                                <li><a class="ur-light" href="<c:url value="/login.html"/>">Sign In</a></li>
                                <li><a class="ur-dark" href="<c:url value="/signup.html"/>">Create Account</a></li>
                            </sec:authorize>
                            <sec:authorize access="hasRole('admin')">
                                <li><a class="ur-light ur-username" href="<c:url value="/admin.html"/>">Admin</a></li>
                            </sec:authorize>
                            <sec:authorize access="isAuthenticated()">
                                <li><a class="ur-light ur-username" href="<c:url value="/account.html"/>"><sec:authentication property="principal.username"/></a></li>
                                <li><a class="ur-dark" href="<c:url value="/j_spring_security_logout"/>">Log Out</a></li>
                            </sec:authorize>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <tiles:insertAttribute name="body"/>

        <footer>
            <p><span class="brand ur-brand ur-light">uno<span>.</span>reader</span></p>
            <ul>
                <li><a href="https://bitbucket.org/ideasagiles/unoreader/issues/new">Report a Bug</a></li>
                <li><a href="https://bitbucket.org/ideasagiles/unoreader">Source Code</a></li>
                <li><a href="mailto:leonardo.deseta@ideasagiles.com">Contact</a></li>
            </ul>
        </footer>

        <script src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
        <script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
        <script src="https://www.google.com/jsapi"></script>
        <script>google.load("feeds", "1");</script>

        <script src="js/lib/async/async.js"></script>
        <script src="js/lib/jquery-context-menu/jquery.contextMenu.js"></script>
        <script src="js/lib/jquery-custom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="js/lib/jquery-noty/jquery.noty.js"></script>
        <script src="js/lib/jquery-noty/layouts/top.js"></script>
        <script src="js/lib/jquery-noty/themes/default.js"></script>
        <script src="js/lib/jquery-taphold/taphold.js"></script>
        <script src="js/lib/jquery-waypoints/waypoints.min.js"></script>
        <script src="js/lib/jsrender/jsrender.js"></script>
        <script src="js/lib/moment/moment.min.js"></script>
        <script src="js/lib/underscore/underscore-min.js"></script>

        <script src="js/app/unoreader.js"></script>
        <script src="js/app/pageiterator/pageIterator.js"></script>
        <script src="js/app/feedmanager/FeedManager.js"></script>
        <script src="js/app/service/service.js"></script>
        <script src="js/app/service/channel/channel.js"></script>
        <script src="js/app/service/favourite/favourite.js"></script>
        <script src="js/app/service/feed/feed.js"></script>
        <script src="js/app/service/passwordrecovery/passwordRecovery.js"></script>
        <script src="js/app/service/user/user.js"></script>
        <script src="js/app/webreader/webreader.js"></script>

        <sec:authorize access="isAuthenticated()">
            <script>
                unoreader.user = { username: '<sec:authentication property="principal.username" htmlEscape="false"/>' };
            </script>
        </sec:authorize>

        <tiles:importAttribute name="pageJs" scope="page"/>
        <c:if test="${not empty pageJs}">
            <script src="<tiles:getAsString name="pageJs"/>"></script>
        </c:if>

    </body>
</html>