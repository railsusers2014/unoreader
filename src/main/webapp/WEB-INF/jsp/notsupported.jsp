<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="container">
    <div class="row">
        <div class="span6 offset3">
            <div class="ur-lightbox ur-extra-padding ur-extra-margin">
                <h3>Your browser is too old and is not secure.</h3>
                <p>Your current web browser is too old and does not support Uno Reader.
                    Please consider upgrading to a newer version, and you will enjoy
                    a safer web experience.
                </p>
                <p>
                    We recommend you to try Mozilla Firefox, a free and secure web browser.
                </p>
                <div class="ur-actions">
                    <div>
                        <a class="btn btn-success" href="http://www.firefox.com">Download Firefox</a>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a class="btn btn-small" href="http://www.google.com/chrome/">Download Chrome</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>