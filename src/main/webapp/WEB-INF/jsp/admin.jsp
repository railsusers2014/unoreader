<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<div class="container">
    <div class="row">
        <div class="span6 offset3">
            <div class="ur-lightbox ur-extra-margin">
                <div class="ur-header"><h3>Users</h3></div>
                <table class="table table-hover" width="100%">
                    <thead>
                        <tr><th>Id</th>
                            <th>Email</th>
                            <th>Member since</th>
                        </tr>
                    </thead>
                    <tbody id="usersList"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!--********************************************************************
                            TEMPLATES
*********************************************************************-->
<script id="usersListTemplate" type="text/x-jsrender">
    <tr>
        <td>{{:id}}</td>
        <td>{{:username}}</td>
        <td>{{formatDate:memberSince}}</td>
    </tr>
</script>