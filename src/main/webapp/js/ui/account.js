/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
var page = (function() {
    function init() {
        $("#accountForm input[name='oldPassword']").val("");
        $("#accountForm input[name='newPassword1']").val("");
        $("#accountForm input[name='newPassword2']").val("");
        $("#accountForm").focus();
        $(document).on("submit", "#accountForm", submitAccountForm);
    }

    function submitAccountForm(e) {
        var user = {};
        var newPassword2 = $("#accountForm input[name='newPassword2']").val();
        user.username = $("#accountForm input[name='username']").val();
        user.oldPassword = $("#accountForm input[name='oldPassword']").val();
        user.newPassword = $("#accountForm input[name='newPassword1']").val();

        newPassword2 = newPassword2.length > 0 ? newPassword2 : undefined;
        user.oldPassword = user.oldPassword.length > 0 ? user.oldPassword : undefined;
        user.newPassword = user.newPassword.length > 0 ? user.newPassword : undefined;

        if (validatePassword(user.oldPassword, user.newPassword, newPassword2)) {
            unoreader.service.user.update(user, saveSuccess, saveError);
        }
        return false;
    }

    function saveSuccess(result) {
        renderMessage({
            cssClass: "alert-success",
            title: "Changes saved."
        });
    }

    function saveError(result) {
        if (result.status === 403) {
            renderMessage({
                cssClass: "alert-error",
                title: "The current password was invalid.",
                text: "Please enter your current password and try again."
            });
        }
        else if (result.status === 409) {
            renderMessage({
                cssClass: "alert-error",
                title: "The username already exists.",
                text: "Please choose a different username and try again."
            });
        }
        else {
            renderMessage({
                cssClass: "alert-error",
                title: "There was a problem processing your request.",
                text: "Please try again in a few minutes."
            });
        }
    }

    function validatePassword(oldPassword, password1, password2) {
        if ((oldPassword || password1 || password2) && !(oldPassword && password1 && password2)) {
            renderMessage({
                cssClass: "alert-error",
                title: "Please enter your current password, and the new one.",
                text: "To change your password you need to enter your current password, and the new one."
            });
            return false;
        }
        if (password1 !== password2) {
            renderMessage({
                cssClass: "alert-error",
                title: "Check your password.",
                text: "Please check that both passwords are identical."
            });
            return false;
        }
        return true;
    }

    function renderMessage(message) {
        $("#infoArea").html($("#errorMessageTemplate").render(message));
    }

    return {
        init: init
    };
})();

$(document).ready(function() {
    page.init();
});