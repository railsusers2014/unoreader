/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
var pageReader = (function() {
    var feedManager;
    var selectedChannel;
    var selectedFeedId;
    var entriesIterator;
    var lastRenderedDate;

    var ENTRIES_PAGE_SIZE = 8;
    var ENTRIES_SEPARATOR_DATE_FORMAT = "ddd[,] D MMMM YYYY";
    var MAX_ENTRIES_SINGLE_FEED = 80;
    var MAX_ENTRIES_MANY_FEEDS = 40;

    function init() {
        pageReader.template.init();
        pageReader.contextMenu.init();
        pageReader.dragSupport.init();
        pageReader.editChannelPanel.init();
        pageReader.editFeedPanel.init();
        pageReader.favourites.init();
        pageReader.feedPack.init();
        pageReader.share.init();
        pageReader.viewport.init();

        $("#channelList").on("click", ".js-channel", clickOnChannel);
        $("#feedList").on("click", ".js-feed", clickOnFeed);
        $(document).on("channelChanged channelDeleted feedChanged feedDeleted", refreshChannelsAndFeedList);

        $("#feedList").html($("#feedListEmptyTemplate").render());
        refreshChannels();
    }


    /* *******************************************************************
     * Finders and attributes
     *********************************************************************/

    function findFeedById(feedId) {
        return feedManager.findFeedById(feedId);
    }

    function getSelectedChannel() {
        return selectedChannel;
    }

    function getSelectedFeedId() {
        return selectedFeedId;
    }


    /* *******************************************************************
     * Refresh feeds
     *********************************************************************/

    function refreshFeeds(callback) {
        var finder = unoreader.user ? "findAll" : "findSampleFeeds";

        unoreader.service.feed[finder](function(feeds) {
            feedManager = new unoreader.FeedManager(feeds);
            if (callback) {
                callback();
            }
        });
    }

    function refreshChannels() {
        refreshFeeds(function() {
            renderChannels();
        });
    }

    function refreshChannelsAndFeedList() {
        refreshFeeds(function() {
            renderChannels();
            renderFeedList();
        });
    }


    /* *******************************************************************
     * Channel Panel
     *********************************************************************/

    function renderChannels() {
        var result = {};
        var channelList = $("#channelList");
        result.channels = feedManager.getChannels();
        channelList.html($("#channelListTemplate").render(result));
        channelList.trigger("refresh");
    }

    function clickOnChannel(event) {
        var channelLink = $(event.currentTarget);
        channelLink.focus();
        selectedChannel = channelLink.data("channel");
        channelLink.parent().siblings().removeClass("active");
        channelLink.parent().addClass("active");
        renderFeedList();
        return false;
    }


    /* *******************************************************************
     * Feed Panel
     *********************************************************************/

    function renderFeedList() {
        var channelWithFeeds = findChannelWithFeeds(selectedChannel);
        var feedList = $("#feedList");
        if (channelWithFeeds.feeds.length > 0) {
            feedList.html($("#feedListTemplate").render(channelWithFeeds));
        }
        else {
            feedList.html($("#feedListEmptyTemplate").render());
        }
        feedList.trigger("refresh");
    }

    function clickOnFeed(event) {
        var feedLink = $(event.currentTarget);
        var options = {};
        var feedId = feedLink.data("feedId");

        feedLink.focus();
        renderStartLoading(feedLink);

        if (feedId) {
            options.feedId = feedId;
            options.error = renderErrorMessage;
            options.maxEntriesPerFeed = MAX_ENTRIES_SINGLE_FEED;
        }
        else if (selectedChannel) {
            options.channel = selectedChannel;
            options.maxEntriesPerFeed = MAX_ENTRIES_MANY_FEEDS;
        }
        else {
            options.maxEntriesPerFeed = MAX_ENTRIES_MANY_FEEDS;
        }

        options.success = function(iterator) {
            entriesIterator = iterator;
            selectedFeedId = feedId;
            feedLink.parent().siblings().removeClass("active");
            feedLink.parent().addClass("active");
            clearFeedEntries();
            appendFeedEntries(entriesIterator.next(ENTRIES_PAGE_SIZE));
        };
        options.complete = function() {
            renderStopLoading(feedLink);
        };

        feedManager.loadFeedEntries(options);

        return false;

        function renderErrorMessage(result) {
            pageReader.message.error({
                title: "D'oh! We could not retrieve the articles from the feed.",
                text: "Maybe the site is down?<br/>The error message was: " + result.error.message
            });
        }
    }

    function findChannelWithFeeds(channel) {
        var result = {};
        if (channel) {
            result.channel = channel;
            result.feeds = feedManager.findFeedsByChannel(channel);
        }
        else {
            result.channel = undefined;
            result.feeds = feedManager.findAllDistinctFeeds();
        }
        return result;
    }

    function renderStartLoading(link) {
        var spinner = link.parent().find("i");
        spinner.addClass("icon-spinner");
        spinner.addClass("icon-spin");
    }

    function renderStopLoading(link) {
        var spinner = link.parent().find("i");
        spinner.removeClass("icon-spinner");
        spinner.removeClass("icon-spin");
    }


    /* *******************************************************************
     * Entry Panel
     *********************************************************************/

    function clearFeedEntries() {
        $(document).scrollTop($("#feedEntries").offset().top - 100);
        $("#feedEntries").empty();
        lastRenderedDate = null;
    }

    function appendFeedEntries(entries) {
        var entryDate;
        var i;

        for (i = 0; i < entries.length; i++) {
            entryDate = moment(entries[i].publishedDate, unoreader.webreader.getDateFormatPattern());
            if ((!lastRenderedDate && entryDate) || (lastRenderedDate && entryDate && !entryDate.isSame(lastRenderedDate, "day"))) {
                $("#feedEntries").append($("#feedEntriesDateTemplate").render(createDateSeparator(entryDate)));
                lastRenderedDate = entryDate;
            }
            $("#feedEntries").append($("#feedEntriesTemplate").render(entries[i]));
        }

        makeRefreshableOnScroll();
    }

    function createDateSeparator(date) {
        var separator = {
            date: "",
            cssClass: ""
        };
        if (moment().isSame(date, "day")) {
            separator.date = "Today.&nbsp;&nbsp;&nbsp;";
            separator.cssClass = "ur-today";
        }
        separator.date += date.format(ENTRIES_SEPARATOR_DATE_FORMAT);
        return separator;
    }

    function makeRefreshableOnScroll() {
        $('.ur-entries-bottom').waypoint("destroy");
        $('.ur-entries-bottom').waypoint(refreshEntriesOnScroll, {offset: "125%"});

        function refreshEntriesOnScroll(direction) {
            if (direction === "down" && entriesIterator && entriesIterator.hasNext()) {
                appendFeedEntries(entriesIterator.next(ENTRIES_PAGE_SIZE));
            }
        }
    }

    return {
        init: init,
        getSelectedFeedId: getSelectedFeedId,
        getSelectedChannel: getSelectedChannel,
        findFeedById: findFeedById
    };
})();



/* *******************************************************************
 * Panel: Add / Edit Feed
 *********************************************************************/
pageReader.editFeedPanel = (function() {

    var options;

    function init() {
        $(document).on("click", "#addFeed", clickOnAddFeed);
        $("#editFeedModal").on("submit", "#editFeedForm", submitForm);
    }

    function clickOnAddFeed() {
        render({
            type: "new",
            selectedChannel: pageReader.getSelectedChannel()
        });
    }

    function render(panelOptions) {
        if (!unoreader.user) {
            $("#loginModal").modal();
            return;
        }
        options = panelOptions;

        //remove all handlers
        $(document).off("submit", "#editFeedForm");

        if (options.type === "edit") {
            prepareFormForEdit();
        }
        else if (options.type === "new") {
            prepareFormForNew();
        }
        else {
            throw {message: "Unknown type"};
        }

        $("#editFeedModal").modal();
    }

    function submitForm() {
        if (options.type === "edit") {
            updateFeedFromForm();
        }
        else if (options.type === "new") {
            addFeedFromForm();
        }
        else {
            throw {message: "Unknown type"};
        }
        return false;
    }


    function prepareFormForEdit() {
        var formModal = {
            title: "Edit feed",
            saveButtonText: "Save Changes"
        };
        $("#editFeedModal").html($("#editFeedModalTemplate").render(formModal));

        var idInput = $("#editFeedForm input[name='id']");
        var urlInput = $("#editFeedForm input[name='url']");
        var nameInput = $("#editFeedForm input[name='name']");
        var channelInput = $("#editFeedForm input[name='channel']");

        idInput.val(options.feed.id);
        urlInput.val(options.feed.url);
        nameInput.val(options.feed.name);
        channelInput.val(options.feed.channel);

        urlInput.change(function() {
            unoreader.webreader.findFeedMetadata(urlInput.val(), function(result) {
                nameInput.val(result.feed.title);
            });
        });
    }

    function prepareFormForNew() {
        var formModal = {
            title: "Add a new feed",
            saveButtonText: "Add Feed"
        };
        $("#editFeedModal").html($("#editFeedModalTemplate").render(formModal));

        var urlInput = $("#editFeedForm input[name='url']");
        var nameInput = $("#editFeedForm input[name='name']");
        var channelInput = $("#editFeedForm input[name='channel']");

        channelInput.val(options.selectedChannel ? options.selectedChannel : "Uncategortized");

        urlInput.change(function() {
            unoreader.webreader.findFeedMetadata(urlInput.val(), function(result) {
                nameInput.val(result.feed.title);
            });
        });
    }

    function updateFeedFromForm() {
        var feed = {};
        feed.id = $("#editFeedForm input[name='id']").val();
        feed.url = $("#editFeedForm input[name='url']").val();
        feed.name = $("#editFeedForm input[name='name']").val();
        feed.channel = $("#editFeedForm input[name='channel']").val();

        unoreader.service.feed.update(feed, updateFeedSuccess, editFeedError);
    }

    function updateFeedSuccess() {
        pageReader.message.success({title: "Feed saved."});
        $(document).trigger("feedChanged");
        $("#editFeedModal").modal("hide");
    }

    function addFeedFromForm() {
        var feed = {};
        feed.url = $("#editFeedForm input[name='url']").val();
        feed.name = $("#editFeedForm input[name='name']").val();
        feed.channel = $("#editFeedForm input[name='channel']").val();

        unoreader.service.feed.save(feed, saveFeedSuccess, editFeedError);
    }

    function saveFeedSuccess() {
        pageReader.message.success({
            title: "Feed saved.",
            text: "You can now access your newsfeed in the channels area."
        });
        $("#editFeedForm input[name='url']").val("");
        $("#editFeedForm input[name='name']").val("");
        $(document).trigger("feedChanged");
    }

    function editFeedError(result) {
        if (result.status === 409) {
            pageReader.message.warning({
                title: "The feed is already in the channel.",
                text: "Keep adding other feeds!"
            });
        }
        else {
            pageReader.message.error({
                title: "Ups! There was a problem saving the feed.",
                text: "We had a problem while saving your feed. Please try again in a few minutes."
            });
        }
    }

    return {
        init: init,
        render: render
    };

})();


/* *******************************************************************
 * Edit Channel Panel
 *********************************************************************/
pageReader.editChannelPanel = (function() {

    var options;

    function init() {
        $("#editChannelModal").on("submit", "#editChannelForm", submitForm);
    }

    function render(panelOptions) {
        if (!unoreader.user) {
            $("#loginModal").modal();
            return;
        }

        options = panelOptions;
        $("#editChannelModal").html($("#editChannelModalTemplate").render(options));
        $("#editChannelModal").modal();
    }

    function submitForm() {
        var updateChannel = {};
        updateChannel.oldChannel = $("#editChannelForm input[name='oldChannel']").val();
        updateChannel.newChannel = $("#editChannelForm input[name='newChannel']").val();

        unoreader.service.channel.rename(updateChannel, updateChannelSuccess, updateChannelError);

        return false;
    }

    function updateChannelSuccess() {
        pageReader.message.success({
            title: "Channel saved."
        });
        $(document).trigger("channelChanged");
        $("#editChannelModal").modal("hide");
    }

    function updateChannelError(result) {
        pageReader.message.error({
            title: "Ups! There was a problem saving the channel.",
            text: "We had a problem while saving your channel. Please try again in a few minutes."
        });
    }

    return {
        init: init,
        render: render
    };
})();


/* *******************************************************************
 * Messages
 *********************************************************************/
pageReader.message = (function() {

    function display(message) {
        var text = "<strong>" + message.title + "</strong>  " + (message.text ? message.text : "");
        noty({
            type: message.type,
            timeout: message.timeout,
            text: text
        });
    }

    function success(message) {
        message.type = "success";
        message.timeout = 3000;
        display(message);
    }

    function warning(message) {
        message.type = "warning";
        message.timeout = 3000;
        display(message);
    }

    function error(message) {
        message.type = "error";
        display(message);
    }

    return {
        success: success,
        warning: warning,
        error: error
    };
})();


/* *******************************************************************
 * Feed Packs
 *********************************************************************/
pageReader.feedPack = (function() {
    function init() {
        $(document).on("click", ".js-add-pack", clickOnAddPack);
    }

    function clickOnAddPack(event) {
        var button;
        var pack;

        if (!unoreader.user) {
            $("#loginModal").modal();
            return;
        }

        button = $(event.currentTarget);
        pack = button.data("pack");

        unoreader.service.feed.savePack(pack, savePackSuccess, savePackError);

        function savePackSuccess() {
            pageReader.message.success({
                title: "Great! The feed pack was added.",
                text: "You can now check new articles from the channel list."
            });
            button.attr('disabled', 'disabled');
            $(document).trigger("feedChanged");
        }

        function savePackError() {
            pageReader.message.error({
                title: "Ups! There was a problem saving the pack.",
                text: "We had a problem while saving your feed. Please try again in a few minutes."
            });
        }
    }

    return {
        init: init
    };
})();


/* *******************************************************************
 * Templates
 *********************************************************************/
pageReader.template = (function() {
    var ENTRIES_DATE_FORMAT = "ddd[,] D MMMM YYYY HH:mm";

    function init() {
        $.views.converters({
            formatDate: function(val) {
                return moment(val, unoreader.webreader.getDateFormatPattern()).format(ENTRIES_DATE_FORMAT);
            },
            formatTimeAgo: function(val) {
                return moment(val, unoreader.webreader.getDateFormatPattern()).fromNow();
            }
        });
        $.views.helpers({
            classForFeedItem: function(feedId) {
                return (feedId === pageReader.getSelectedFeedId()) ? "active" : "";
            },
            classForChannelItem: function(channel) {
                return (channel === pageReader.getSelectedChannel()) ? "active" : "";
            }
        });
    }

    return {
        init: init
    };
})();


/* *******************************************************************
 * Drag and drop for feeds.
 *********************************************************************/
pageReader.dragSupport = (function() {
    function init() {
        $("#channelList").on("refresh", makeChannelsDroppable);
        $("#feedList").on("refresh", makeFeedsDraggable);
    }

    function makeChannelsDroppable() {
        $(".js-channel-item").droppable({
            drop: dropItemOnChannel,
            over: overItemOnChannel,
            out: outItemOnChannel
        });
    }

    function makeFeedsDraggable() {
        $(".js-feed-item").draggable({
            helper: feedItemDraggableHelper,
            appendTo: "body"
        });
    }

    function feedItemDraggableHelper(event) {
        var linkName = $(event.currentTarget).text();
        return '<div class="btn btn-small btn-ur-draggable-item"><i class="icon-link"></i> ' + linkName + '</div>';
    }

    function overItemOnChannel(event, ui) {
        //only highlight if its a feed
        if (ui.draggable.data("feedId")) {
            $(this).addClass("ur-droppable-active");
        }
    }

    function outItemOnChannel(event, ui) {
        $(this).removeClass("ur-droppable-active");
    }

    function dropItemOnChannel(event, ui) {
        if (!unoreader.user) {
            $("#loginModal").modal();
            return;
        }

        var channel;
        var feedId = ui.draggable.data("feedId");

        if (feedId) {
            channel = $(this).data("channel");
            dropFeedItemOnChannel(feedId, channel);
            return;
        }
    }

    function dropFeedItemOnChannel(feedId, channel) {
        var feed = {id: feedId, channel: channel};

        unoreader.service.feed.update(feed, moveFeedSuccess, updateFeedError);

        function moveFeedSuccess() {
            pageReader.message.success({
                title: "Feed moved into channel " + channel + "."
            });
            $(document).trigger("feedChanged");
        }

        function updateFeedError() {
            pageReader.message.error({
                title: "D'oh! We could not update your feed.",
                text: "Please try again in a few minutes."
            });
        }
    }

    return {
        init: init
    };
})();


/* *******************************************************************
 * Share
 *********************************************************************/
pageReader.share = (function() {
    function init() {
        $("#feedEntries").on("click", ".js-share-facebook", shareOnFacebook);
        $("#feedEntries").on("click", ".js-share-twitter", shareOnTwitter);
    }

    function shareOnFacebook() {
        var feedLink = $(this).data("link");
        var url = "http://www.facebook.com/sharer.php?u=" + encodeURI(feedLink);
        window.open(url, "Share on Facebook", "height=400,width=600,status=no,location=no");

        return false;
    }

    function shareOnTwitter() {
        var feedLink = $(this).data("link");
        var text = "Check this out! " + feedLink + " (via Uno Reader)";
        var url = "http://twitter.com/?status=" + encodeURI(text);
        window.open(url, "Tweet this", "height=280,width=500,status=no,location=no");

        return false;
    }

    return {
        init: init
    };
})();



/* *******************************************************************
 * Viewport scroll and panel position management
 *********************************************************************/
pageReader.viewport = (function() {
    var MIN_HEIGHT = 100;
    var SCROLL_OPTIONS = {
        theme: "dark-thin",
        scrollButtons: {enable: true},
        autoHideScrollbar: true,
        scrollInertia: 150,
        contentTouchScroll: true
    };

    function init() {
        adjustPanelsHeight();
        $(window).on("scroll", adjustPanelsPositionOnScroll);
        $(window).on("resize", adjustPanelsHeight);
        $("#channelList").on("refresh", adjustChannelPanelHeight);
        $("#feedList").on("refresh", adjustFeedPanelHeight);
    }

    /**
     * Adjusts the "fixed" position for the channels and feed list panels.
     * This function "fixes" the panel if it fits in the current window (and
     * no scrolling is requiered to see all its content), and adjusts the
     * scrollbars for each panel.
     */
    function adjustPanelsHeight() {
        adjustChannelPanelHeight();
        adjustFeedPanelHeight();
    }

    function adjustChannelPanelHeight() {
        var channelScrollContainer = $("#channelListScrollContainer");
        var windowHeight = $(window).height();
        var channelListHeight = channelScrollContainer.height();
        var marginChannelList = 250;
        var maxHeight;

        if (channelListHeight + marginChannelList > windowHeight) {
            maxHeight = windowHeight - marginChannelList;
            if (maxHeight < MIN_HEIGHT) {
                maxHeight = MIN_HEIGHT;
            }
            channelScrollContainer.css("max-height", maxHeight);
        }
        else {
            channelScrollContainer.css("max-height", "");
        }

        channelScrollContainer.mCustomScrollbar("destroy");
        channelScrollContainer.mCustomScrollbar(SCROLL_OPTIONS);
    }

    function adjustFeedPanelHeight() {
        var feedListScrollContainer = $("#feedListScrollContainer");
        var windowHeight = $(window).height();
        var feedListHeight = feedListScrollContainer.height();
        var marginFeedList = 200;
        var maxHeight;

        if (feedListHeight + marginFeedList > windowHeight) {
            maxHeight = windowHeight - marginFeedList;
            if (maxHeight < MIN_HEIGHT) {
                maxHeight = MIN_HEIGHT;
            }
            feedListScrollContainer.css("max-height", maxHeight);
        }
        else {
            feedListScrollContainer.css("max-height", "");
        }

        feedListScrollContainer.mCustomScrollbar("destroy");
        feedListScrollContainer.mCustomScrollbar(SCROLL_OPTIONS);
    }

    /**
     * Adjusts the "top" position for the channels and feed list panels.
     */
    function adjustPanelsPositionOnScroll() {
        var initialTop = 96; //initial position for panels
        var minTop = 30;     //minimun desired top value
        var scrollTop = $(document).scrollTop();
        var top = initialTop - scrollTop;
        if (top < minTop) {
            top = minTop;
        }
        $("#channelListFixedContainer").css("top", top);
        $("#feedListFixedContainer").css("top", top);
    }

    return {
        init: init
    };
})();


/* *******************************************************************
 * Favourites
 *********************************************************************/
pageReader.favourites = (function() {
    var favouriteList;

    function init() {
        $("#channelList").on("click", ".js-favourites", clickOnFavourites);
        $("#feedEntries").on("click", ".js-favourite-entry", clickOnMakeFavouriteEntry);
        $("#feedEntries").on("click", ".js-favourite-remove", clickOnRemoveFavourite);
        $("#feedEntries").on("mouseover", ".js-favourite-entry", overMakeFavouriteEntry);
        $("#feedEntries").on("mouseout", ".js-favourite-entry", outMakeFavouriteEntry);
    }

    function clickOnFavourites(event) {
        if (!unoreader.user) {
            $("#loginModal").modal();
            return;
        }

        var favouriteLink = $(event.currentTarget);
        favouriteLink.parent().siblings().removeClass("active");
        favouriteLink.parent().addClass("active");

        $("#feedList").html($("#favouriteListEmptyTemplate").render());
        refreshFavouriteList();
        return false;
    }

    function clickOnMakeFavouriteEntry(event) {
        if (!unoreader.user) {
            $("#loginModal").modal();
            return;
        }

        var favouriteLink = $(event.currentTarget);
        var favourite = {
            title: favouriteLink.data("title"),
            url: favouriteLink.data("link"),
            snippet: favouriteLink.data("snippet")
        };
        unoreader.service.favourite.save(favourite, saveFavouriteSuccess, saveFavouriteError);

        return false;

        function saveFavouriteSuccess() {
            favouriteLink.data("active", true);
            pageReader.message.success({
                title: "Article saved as favourite.",
                text: "You can check all your favourite articles in the Favourites channel."
            });
        }

        function saveFavouriteError(result) {
            if (result.status === 409) {
                favouriteLink.data("active", true);
                pageReader.message.warning({
                    title: "Article is already a favourite."
                });
            }
            else {
                pageReader.message.error({
                    title: "Ups! We could not save the favourite.",
                    text: "Please try again in a few minutes."
                });
            }
        }
    }

    function clickOnRemoveFavourite(event) {
        var favouriteLink = $(event.currentTarget);
        var id = favouriteLink.data("id");
        unoreader.service.favourite.deleteFavourite(id, deleteFavouriteSuccess, deleteFavouriteError);

        return false;
    }

    function deleteFavouriteSuccess() {
        pageReader.message.success({
            title: "Favourite removed."
        });
        refreshFavouriteList();
    }

    function deleteFavouriteError() {
        pageReader.message.error({
            title: "Ups! We could not delete the favourite.",
            text: "Please try again in a few minutes"
        });
    }

    function refreshFavouriteList() {
        unoreader.service.favourite.findAll(function(favourites) {
            favouriteList = favourites;
            $("#feedEntries").html($("#favouriteListTemplate").render(favouriteList));
        }, function() {
            pageReader.message.error({
                title: "D'oh! We could not retrieve the favourites.",
                text: "Please try again in a few minutes."
            });
        });
    }

    function overMakeFavouriteEntry(event) {
        var favouriteLink = $(event.currentTarget);
        var icon = favouriteLink.find("i");
        icon.removeClass("icon-star-empty");
        icon.addClass("icon-star");
    }

    function outMakeFavouriteEntry(event) {
        var icon;
        var favouriteLink = $(event.currentTarget);
        var active = favouriteLink.data("active");
        if (!active) {
            icon = favouriteLink.find("i");
            icon.removeClass("icon-star");
            icon.addClass("icon-star-empty");
        }
    }

    return {
        init: init
    };
})();


/* *******************************************************************
 * Context menu for edit/delete
 *********************************************************************/
pageReader.contextMenu = (function() {
    function init() {
        $("#confirmDeleteChannelModal").on("click", "#confirmDeleteChannelButton", deleteChannel);

        $("#channelList").on("taphold", ".js-channel-item", {duration: 800}, function(event) {
            var target = $(event.currentTarget);
            target.focus();
            target.contextMenu();
            return false;
        });

        $("#feedList").on("taphold", ".js-feed-item", {duration: 800}, function(event) {
            var target = $(event.currentTarget);
            target.focus();
            target.contextMenu();
            return false;
        });

        $("#channelList").contextMenu({
            selector: ".js-channel-item",
            callback: function(menuKey) {
                var channel = $(this).data("channel");
                switch (menuKey) {
                    case "edit":
                        editChannel(channel);
                        break;
                    case "delete":
                        confirmDeleteChannel(channel);
                        break;
                }
            },
            items: {
                "edit": {name: "<i class='icon-pencil'></i>&nbsp;&nbsp;&nbsp; Edit"},
                "delete": {name: "<i class='icon-trash'></i>&nbsp;&nbsp;&nbsp; Delete"}
            }
        });

        $("#feedList").contextMenu({
            selector: ".js-feed-item",
            callback: function(menuKey) {
                var feedId = $(this).data("feedId");
                switch (menuKey) {
                    case "edit":
                        editFeed(feedId);
                        break;
                    case "delete":
                        deleteFeed(feedId);
                        break;
                }
            },
            items: {
                "edit": {name: "<i class='icon-pencil'></i>&nbsp;&nbsp;&nbsp; Edit"},
                "delete": {name: "<i class='icon-trash'></i>&nbsp;&nbsp;&nbsp; Delete"}
            }
        });
    }

    function editFeed(feedId) {
        var feed = pageReader.findFeedById(feedId);
        pageReader.editFeedPanel.render({
            type: "edit",
            feed: feed
        });
    }

    function deleteFeed(feedId) {
        unoreader.service.feed.deleteFeed(feedId, deleteFeedSuccess, deleteFeedError);

        function deleteFeedSuccess() {
            pageReader.message.success({title: "Feed deleted."});
            $(document).trigger("feedDeleted");
        }
        function deleteFeedError() {
            pageReader.message.error({
                title: "D'oh! We could not delete your feed.",
                text: "Please try again in a few minutes."
            });
        }
    }

    function editChannel(channel) {
        pageReader.editChannelPanel.render({
            channel: channel
        });
    }

    function confirmDeleteChannel(channel) {
        $("#confirmDeleteChannelModal").html($("#confirmDeleteChannelModalTemplate").render(channel));
        $("#confirmDeleteChannelModal").modal();
    }

    function deleteChannel(event) {
        var channel = $(event.currentTarget).data("channel");
        unoreader.service.channel.deleteChannelAndFeeds(channel, deleteChannelSuccess, deleteChannelError);

        function deleteChannelSuccess() {
            pageReader.message.success({title: "Channel " + channel + " and all its feeds deleted."});
            $("#confirmDeleteChannelModal").modal("hide");
            $(document).trigger("channelDeleted");
        }
        function deleteChannelError() {
            pageReader.message.error({
                title: "D'oh! We could not delete the channel.",
                text: "Please try again in a few minutes."
            });
        }
    }

    return {
        init: init
    };
})();


/* *******************************************************************
 * Start app.
 *********************************************************************/
$(document).ready(function() {
    pageReader.init();
});