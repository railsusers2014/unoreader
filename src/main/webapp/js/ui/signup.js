/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
var page = (function() {
    function init() {
        $("#signupForm").focus();
        $(document).on("submit", "#signupForm", submitSignupForm);
    }

    function submitSignupForm(e) {
        var user = {};
        var password2 = $("#signupForm input[name='password2']").val();
        user.username = $("#signupForm input[name='username']").val();
        user.password = $("#signupForm input[name='password']").val();

        if (validatePassword(user.password, password2)) {
            unoreader.service.user.save(user, saveSuccess, saveError);
        }
        return false;
    }

    function saveSuccess(result) {
        window.location.href = unoreader.baseURI + "reader.html";
    }

    function saveError(result) {
        if (result.status === 409) {
            renderErrorMessage({
                title: "The username already exists.",
                text: "Please choose a different username and try again."
            });
        }
        else {
            renderErrorMessage({
                title: "There was a problem processing your request.",
                text: "Please try again in a few minutes."
            });
        }
    }

    function validatePassword(password1, password2) {
        if (password1 !== password2) {
            renderErrorMessage({
                title: "Check your password.",
                text: "Please check that both passwords are identical."
            });
            return false;
        }
        return true;
    }

    function renderErrorMessage(message) {
        $("#infoArea").html($("#errorMessageTemplate").render(message));
    }

    return {
        init: init
    };
})();

$(document).ready(function() {
    page.init();
});