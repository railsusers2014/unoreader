/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
unoreader.service.feed = (function() {
    function findSampleFeeds(onSuccessCallback) {
        var result = [
            {
                id: 1,
                url: "http://feed.500px.com/500px-best",
                name: "500px",
                channel: "art & photos"
            },
            {
                id: 2,
                url: "http://www.flourish.org/news/flickr-daily-interesting.xml",
                name: "Daily Flickr",
                channel: "art & photos"
            },
            {
                id: 3,
                url: "http://www.joystiq.com/rss.xml",
                name: "Joystiq",
                channel: "videogames"
            },
            {
                id: 4,
                url: "http://www.destructoid.com/?mode=atom",
                name: "Destructoid",
                channel: "videogames"
            },
            {
                id: 5,
                url: "http://feeds.feedburner.com/RockPaperShotgun",
                name: "Rock, Paper, Shotgun",
                channel: "videogames"
            },
            {
                id: 6,
                url: "http://feeds.arstechnica.com/arstechnica/index",
                name: "Ars Technica",
                channel: "technology"
            },
            {
                id: 7,
                url: "http://www.engadget.com/rss.xml",
                name: "Engadget",
                channel: "technology"
            },
            {
                id: 8,
                url: "http://feeds.feedburner.com/Techcrunch",
                name: "TechCrunch",
                channel: "technology"
            }
,
            {
                id: 9,
                url: "http://feeds.bbci.co.uk/news/rss.xml",
                name: "BBC",
                channel: "world news"
            },
            {
                id: 10,
                url: "http://rss.cnn.com/rss/edition.rss",
                name: "CNN",
                channel: "world news"
            },
            {
                id: 11,
                url: "http://feeds.reuters.com/reuters/worldNews",
                name: "Reuters",
                channel: "world news"
            },
            {
                id: 12,
                url: "http://sports-ak.espn.go.com/espn/rss/news",
                name: "ESPN",
                channel: "sports"
            },
            {
                id: 13,
                url: "http://feeds.bbci.co.uk/sport/0/rss.xml",
                name: "BBC Sport",
                channel: "sports"
            },
            {
                id: 14,
                url: "http://feeds.pheedo.com/feedout/syndicatedContent_categoryId_0",
                name: "Fox Sports",
                channel: "sports"
            }
        ];

        onSuccessCallback(result);
    }

    function findAll(successCallback, errorCallback) {
        var url = unoreader.service.baseURI + "/feeds";
        $.getJSON(url)
                .done(successCallback)
                .fail(errorCallback);
    }

    function save(feed, successCallback, errorCallback) {
        var url = unoreader.service.baseURI + "/feeds";
        unoreader.service.postJSON({
            url: url,
            data: feed,
            success: successCallback,
            error: errorCallback
        });
    }

    function update(feed, successCallback, errorCallback) {
        var url = unoreader.service.baseURI + "/feeds";
        unoreader.service.putJSON({
            url: url,
            data: feed,
            success: successCallback,
            error: errorCallback
        });
    }

    function deleteFeed(feedId, successCallback, errorCallback) {
        var url = unoreader.service.baseURI + "/feeds/" + feedId;
        unoreader.service.deleteJSON({
            url: url,
            success: successCallback,
            error: errorCallback
        });
    }

    function savePack(packName, successCallback, errorCallback) {
        var url = unoreader.service.baseURI + "/feeds/pack/" + packName;
        unoreader.service.postJSON({
            url: url,
            data: packName,
            success: successCallback,
            error: errorCallback
        });
    }

    return {
        findAll: findAll,
        findSampleFeeds: findSampleFeeds,
        save: save,
        deleteFeed: deleteFeed,
        update: update,
        savePack: savePack
    };
})();