/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
unoreader.service.channel = (function() {

    function rename(channelUpdate, successCallback, errorCallback) {
        var url = unoreader.service.baseURI + "/channels";
        unoreader.service.putJSON({
            url: url,
            data: channelUpdate,
            success: successCallback,
            error: errorCallback
        });
    }

    function deleteChannelAndFeeds(channel, successCallback, errorCallback) {
        var url = unoreader.service.baseURI + "/channels/" + encodeURI(channel);
        unoreader.service.deleteJSON({
            url: url,
            success: successCallback,
            error: errorCallback
        });
    }

    return {
        rename: rename,
        deleteChannelAndFeeds: deleteChannelAndFeeds
    };
})();