/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
unoreader.service.user = (function() {
    function save(user, successCallback, errorCallback) {
        var url = unoreader.service.baseURI + "/user";
        unoreader.service.postJSON({
            url: url,
            data: user,
            success: successCallback,
            error: errorCallback
        });
    }

    function update(user, successCallback, errorCallback) {
        var url = unoreader.service.baseURI + "/user";
        unoreader.service.putJSON({
            url: url,
            data: user,
            success: successCallback,
            error: errorCallback
        });
    }

    function findAll(successCallback, errorCallback) {
        var url = unoreader.service.baseURI + "/user";
        $.getJSON(url)
                .done(successCallback)
                .fail(errorCallback);
    }

    return {
        save: save,
        update: update,
        findAll: findAll
    };
})();