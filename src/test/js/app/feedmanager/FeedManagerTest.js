/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var feedManager;

module("unoreader.FeedManager Tests", {
    setup: function() {
        new unoreader.FeedManager(findTestFeeds());
    }
});

test("constructor, with new, should create instance", function() {
    feedManager = new unoreader.FeedManager([]);
    ok(feedManager && typeof feedManager === "object", "Must be an object");
    ok($.isFunction(feedManager.getChannels), "getChannels must be a Function");
});

test("constructor, without new, should create instance", function() {
    feedManager = unoreader.FeedManager([]);
    ok(feedManager && typeof feedManager === "object", "Must be an object");
    ok($.isFunction(feedManager.getChannels), "getChannels must be a Function");
});

test("getChannels", function() {
    feedManager = new unoreader.FeedManager(findTestFeeds());

    var channels = feedManager.getChannels();
    ok(channels.length === 3, "There must be channels");
    ok(isOrderedAsc(channels), "The array must be ordered");
});

test("findFeedsByChannel, id exists, returns feeds", function() {
    var channel = "games";
    feedManager = new unoreader.FeedManager(findTestFeeds());

    var feeds = feedManager.findFeedsByChannel(channel);

    ok(feeds.length === 3, "There should be 3 results");
    ok(isOrderedByName(feeds), "The array must be ordered by name");
    ok(feeds[0].channel === channel, "The feed must be from the given channel");
    ok(feeds[1].channel === channel, "The feed must be from the given channel");
    ok(feeds[2].channel === channel, "The feed must be from the given channel");
    ok(feeds[2].id === 5, "The last feeds should be Rock Paper Shotgun");
    ok(feeds[2].name === "Rock, Paper, Shotgun", "The last feeds should be Rock Paper Shotgun");
});

test("findFeedsByChannel, id does not exists, returns empty Array", function() {
    var channel = "does not exists";
    feedManager = new unoreader.FeedManager(findTestFeeds());

    var feeds = feedManager.findFeedsByChannel(channel);
    ok(feeds, "Feed must not be null");
    ok(feeds.length === 0, "Feed must be an empty array");
});

test("findAllDistinctFeeds, returns array", function() {
    feedManager = new unoreader.FeedManager(findTestFeeds());

    var feeds = feedManager.findAllDistinctFeeds();
    ok(feeds.length > 0, "There must be feeds");
    ok(feeds[0].id === 6, "The first feed must be Ars Technica");
    ok(feeds[0].name === "Ars Technica", "The first feed must be Ars Technica");
});

asyncTest("loadFeedEntries, one feed with valid id, invokes success callback", 6, function() {
    feedManager = new unoreader.FeedManager(findTestFeeds());
    feedManager.loadFeedEntries({feedId: 5, maxEntriesPerFeed: 20, success: successCallback, error: errorCallback});

    function successCallback(iterator) {
        var entries = iterator.next(100);
        ok(entries.length === 20, "There must be new and historical entries");
        ok(isOrderedByPublishedDate(entries), "The array must be ordered by published date");
        ok(entries[0].title, "There must be a title");
        ok(entries[0].link, "There must be a link");
        ok(entries[0].publishedDate, "There must be a published date");
        ok(entries[0].content, "There must be content");
        start();
    }
    function errorCallback() {
        ok(false, "error callback invoked instead of success callback");
        start();
    }
});

asyncTest("loadFeedEntries, one feed with invalid id, invokes error callback", 2, function() {
    feedManager = new unoreader.FeedManager(findTestFeeds());

    feedManager.loadFeedEntries({feedId: 1, success: successCallback, error: errorCallback});

    function successCallback() {
        ok(false, "success callback invoked instead of error callback");
        start();
    }
    function errorCallback(result) {
        ok(result, "the error object must not be null");
        ok(result.error, "the error object must have a message");
        start();
    }
});

asyncTest("loadFeedEntries, one channel with valid id and some invalid feeds and no error callback, invokes success callback with results", 6, function() {
    feedManager = new unoreader.FeedManager(findTestFeeds());

    feedManager.loadFeedEntries({channelId: 1, success: successCallback});

    function successCallback(iterator) {
        var entries = iterator.next(100);
        ok(entries.length > 10, "There must be new and historical entries");
        ok(isOrderedByPublishedDate(entries), "The array must be ordered by published date");
        ok(entries[0].title, "There must be a title");
        ok(entries[0].link, "There must be a link");
        ok(entries[0].publishedDate, "There must be a published date");
        ok(entries[0].content, "There must be content");
        start();
    }
});

asyncTest("loadFeedEntries, one channel with invalid id and no error callback, invokes success callback with empty array", 1, function() {
    feedManager = new unoreader.FeedManager(findTestFeeds());

    feedManager.loadFeedEntries({channel: "does no exist", success: successCallback});

    function successCallback(iterator) {
        var entries = iterator.next(100);
        ok(entries.length === 0, "there must be no entries.");
        start();
    }
});

asyncTest("loadFeedEntries, one channel with invalid feeds, invokes error callback", 2, function() {
    feedManager = new unoreader.FeedManager(findTestFeeds());

    feedManager.loadFeedEntries({feedId: 1, success: successCallback, error: errorCallback});

    function successCallback() {
        ok(false, "success callback invoked instead of error callback");
        start();
    }
    function errorCallback(result) {
        ok(result, "the error object must not be null");
        ok(result.error, "the error object must have a message");
        start();
    }
});

asyncTest("loadFeedEntries, empty filter and some invalid feeds, invokes success callback with entries", 6, function() {
    feedManager = new unoreader.FeedManager(findTestFeeds());

    feedManager.loadFeedEntries({maxEntriesPerFeed: 20, success: successCallback});

    function successCallback(iterator) {
        var entries = iterator.next(100);
        ok(entries.length > 20, "There must be new and historical entries");
        ok(isOrderedByPublishedDate(entries), "The array must be ordered by published date");
        ok(entries[0].title, "There must be a title");
        ok(entries[0].link, "There must be a link");
        ok(entries[0].publishedDate, "There must be a published date");
        ok(entries[0].content, "There must be content");
        start();
    }
});

asyncTest("loadFeedEntries, empty filter and some invalid feeds and error callback, invokes error callback with result", 2, function() {
    feedManager = new unoreader.FeedManager(findTestFeeds());

    feedManager.loadFeedEntries({success: successCallback, error: errorCallback});

    function successCallback() {
        ok(false, "success callback invoked instead of error callback");
        start();
    }
    function errorCallback(result) {
        ok(result, "the error object must not be null");
        ok(result.error, "the error object must have a message");
        start();
    }
});

/* *******************************************************************
 * Utils
 ********************************************************************/
function isOrderedByPublishedDate(array) {
    var i;
    var orderedArray = array.slice();
    orderedArray.sort(function(a, b) {
        var dateA = new Date(a.publishedDate);
        var dateB = new Date(b.publishedDate);
        return dateA.getTime() < dateB.getTime();
    });

    for (i = 0; i < array.length; i++) {
        if (array[i] !== orderedArray[i]) {
            return false;
        }
    }
    return true;
}

function isOrderedByName(array) {
    var i;
    var orderedArray = array.slice();
    orderedArray.sort(function(a, b) {
        return a.name > b.name;
    });

    for (i = 0; i < array.length; i++) {
        if (array[i] !== orderedArray[i]) {
            return false;
        }
    }
    return true;
}

function isOrderedAsc(array) {
    var i;
    var orderedArray = array.slice();
    orderedArray.sort(function(a, b) {
        return a > b;
    });

    for (i = 0; i < array.length; i++) {
        if (array[i] !== orderedArray[i]) {
            return false;
        }
    }
    return true;

}

function findTestFeeds() {
    var result = [
        {
            id: 1,
            url: "http://www.theserverside.com/rss",
            name: "The Server Side",
            channel: "software development"
        },
        {
            id: 2,
            url: "http://www.dosideas.com/noticias.feed?type=rss",
            name: "Dos Ideas",
            channel: "software development"
        },
        {
            id: 3,
            url: "http://feeds.ign.com/ign/games-articles?format=xml",
            name: "IGN - Video Games",
            channel: "games"
        },
        {
            id: 4,
            url: "http://feeds.ign.com/ign/pc-all?format=xml",
            name: "IGN - PC",
            channel: "games"
        },
        {
            id: 5,
            url: "http://feeds.feedburner.com/RockPaperShotgun",
            name: "Rock, Paper, Shotgun",
            channel: "games"
        },
        {
            id: 6,
            url: "http://feeds.arstechnica.com/arstechnica/index",
            name: "Ars Technica",
            channel: "technology"
        }

    ];

    return result;
}