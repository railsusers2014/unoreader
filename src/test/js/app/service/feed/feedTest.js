/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

asyncTest("findTestFeeds, returns feeds Array", 46, function() {
    unoreader.service.feed.findSampleFeeds(function(feeds) {
        var i;
        ok(feeds, "feeds must not be null or undefined");
        ok(feeds.length > 1, "there should be many feeds");
        for (i = 0; i < feeds.length; i++) {
            ok(feeds[i].id, "the id must be setted");
            ok(feeds[i].url, "the url must be setted");
            ok(feeds[i].name, "the name must be setted");
            ok(feeds[i].channel, "the channel must be setted");
        }
        start();
    });
});