/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader.service;

import com.ideasagiles.reader.AbstractDatabaseTest;
import com.ideasagiles.reader.SecurityTestUtils;
import com.ideasagiles.reader.domain.Feed;
import com.ideasagiles.reader.domain.User;
import com.ideasagiles.reader.repository.FeedRepository;
import java.util.List;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import static org.junit.Assert.*;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;

@Transactional
public class ChannelServiceTest extends AbstractDatabaseTest {

    @Autowired
    private ChannelService channelService;
    @Autowired
    private FeedRepository feedRepository;
    @Autowired
    private UserService userService;

    @Test
    public void rename_oldNameExists_renamesChannels() {
        SecurityTestUtils.loginTestUser();
        User user = userService.findLoggedInUser();
        String oldName = "games";
        String newName = " New Channel Name ";

        channelService.rename(oldName, newName);

        String expectedChannel = newName.trim().toLowerCase();
        List<Feed> feeds = feedRepository.findByChannelAndUser(expectedChannel, user);
        assertTrue(feeds.size() > 0);
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void rename_userIsNotLoggedIn_throwsSecurityException() {
        channelService.rename("old", "new");
    }

    @Test
    public void deleteChannelAndFeeds_channelExists_deletesChannelAndFeeds() {
        SecurityTestUtils.loginTestUser();
        User user = userService.findLoggedInUser();
        String channel = "games";

        channelService.deleteChannelAndFeeds(channel);

        List<Feed> feeds = feedRepository.findByChannelAndUser(channel, user);
        assertTrue(feeds.isEmpty());
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void deleteChannelAndFeeds_userIsNotLoggedIn_throwsSecurityException() {
        channelService.deleteChannelAndFeeds("games");
    }

}
