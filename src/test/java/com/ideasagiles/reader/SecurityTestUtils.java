/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * This utility class contains many useful security calls to authenticate
 * users in diferent scenarios.
 */
public class SecurityTestUtils {
    public static final String USER_DEFAULT = "leonardo.deseta@ideasagiles.com";
    public static final String PASSWORD_DEFAULT = "test";
    public static final String USER_ADMIN = "admin@ideasagiles.com";
    public static final String PASSWORD_ADMIN = "test";

    /** Logs in with a user that has standard user roles assigned. */
    public static void loginTestUser() {
        login(USER_DEFAULT, PASSWORD_DEFAULT);
    }

    /** Logs in with a user that has standard user roles assigned. */
    public static void loginAdminUser() {
        login(USER_ADMIN, PASSWORD_ADMIN);
    }

    public static void logout() {
        SecurityContextHolder.clearContext();
    }

    public static void login(String username, String password) {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
        SecurityContextHolder.getContext().setAuthentication(token);
    }
}
