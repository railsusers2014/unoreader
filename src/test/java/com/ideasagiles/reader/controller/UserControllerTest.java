/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader.controller;

import com.ideasagiles.reader.AbstractDatabaseTest;
import com.ideasagiles.reader.ConversionTestUtils;
import com.ideasagiles.reader.domain.User;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebAppConfiguration
public class UserControllerTest extends AbstractDatabaseTest {

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void save_validUser_returnsUserWithId() throws Exception {
        User user = new User();
        user.setUsername("a-test-user@ideasagiles.com");
        user.setPassword("password");

        this.mockMvc.perform(post("/user").content(ConversionTestUtils.toJSON(user))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"));
    }

    @Test
    public void save_usernameExists_returnsStatus409() throws Exception {
        User user = new User();
        user.setUsername("leonardo.deseta@ideasagiles.com");
        user.setPassword("password");

        this.mockMvc.perform(post("/user").content(ConversionTestUtils.toJSON(user))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(409));
    }

}
